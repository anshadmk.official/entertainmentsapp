//
//  PopularMoviesWebService.swift
//  EntertainmentsApp
//
//  Created by Support on 02/02/20.
//  Copyright © 2020 Support. All rights reserved.
//

import UIKit

class MoviesListWebServices: NSObject {

    func getPopularMovies(pageNumber:Int,completion:@escaping (_ movies:[MovieModel]) -> Void){
        
        
        let token = (UserDefaults.standard.value(forKey: "Token") != nil) ? UserDefaults.standard.value(forKey: "Token") as! String  : ""
        var baseUrl = "https://api.themoviedb.org/3/movie/popular"
        baseUrl.append("?api_key=\(token)")
        baseUrl.append("&language=en-US")
        baseUrl.append("&page=2")
        

        
        NetworkManager.shared.getRequest(url: baseUrl) { (response:Any?, error:String?) in

            let movies = Movies.initWithData(data: response as! Data)
            completion((movies?.results!)!)
        }
    }
    
    
    func getTopRatedMovies(pageNumber:Int,completion:@escaping (_ movies:[MovieModel]) -> Void){
        
        
        let token = (UserDefaults.standard.value(forKey: "Token") != nil) ? UserDefaults.standard.value(forKey: "Token") as! String  : ""
        var baseUrl = "https://api.themoviedb.org/3/movie/top_rated"
        baseUrl.append("?api_key=\(token)")
        baseUrl.append("&language=en-US")
        baseUrl.append("&page=2")
        
        
        NetworkManager.shared.getRequest(url: baseUrl) { (response:Any?, error:String?) in

            let movies = Movies.initWithData(data: response as! Data)
            completion((movies?.results!)!)
        }
        
    }
    
    
    
    
    func getUpComingMovies(pageNumber:Int,completion:@escaping (_ movies:[MovieModel]) -> Void){
        
        let token = (UserDefaults.standard.value(forKey: "Token") != nil) ? UserDefaults.standard.value(forKey: "Token") as! String  : ""
        var baseUrl = "https://api.themoviedb.org/3/movie/upcoming"
        baseUrl.append("?api_key=\(token)")
        baseUrl.append("&language=en-US")
        baseUrl.append("&page=2")

        
        NetworkManager.shared.getRequest(url: baseUrl) { (response:Any?, error:String?) in

            let movies = Movies.initWithData(data: response as! Data)
            completion((movies?.results!)!)
        }
        
    }

}
