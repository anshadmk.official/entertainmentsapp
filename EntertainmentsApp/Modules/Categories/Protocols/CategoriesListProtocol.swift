//
//  CategoriesListProtocol.swift
//  EntertainmentsApp
//
//  Created by Support on 02/02/20.
//  Copyright © 2020 Support. All rights reserved.
//

import UIKit

protocol ViewToPresenterCategoriesListProtocol: class{
    
    
    var view: PresenterToViewCategoriesListProtocol? {get set}
    var interactor: PresenterToInteractorCategoriesListProtocol? {get set}
    var router: PresenterToRouterCategoriesListProtocol? {get set}
    
    func fetchCategories()
    func displayShowsList(navigationController:UINavigationController)

}

protocol PresenterToViewCategoriesListProtocol: class{
    func displayCategories(itemsArray:[String])
    func displayError()
}

protocol PresenterToRouterCategoriesListProtocol: class {
    static func createModuleScreen() -> CategoriesVC
    func pushToShowListScreen(navigationConroller:UINavigationController)
}

protocol PresenterToInteractorCategoriesListProtocol: class {
    var presenter:InteractorToPresenterCategoriesListProtocol? {get set}
    func fetchCategoryList()
}

protocol InteractorToPresenterCategoriesListProtocol: class {
    func categoriesFetchedSuccess(categoriesArray:[String])
    func categoriesFetchFailed()
}
