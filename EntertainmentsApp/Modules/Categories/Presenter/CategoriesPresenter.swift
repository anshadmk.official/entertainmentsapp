//
//  CategoriesPresenter.swift
//  EntertainmentsApp
//
//  Created by Support on 02/02/20.
//  Copyright © 2020 Support. All rights reserved.
//

import UIKit

class CategoriesPresenter: ViewToPresenterCategoriesListProtocol {
    
    var view: PresenterToViewCategoriesListProtocol?
    
    var interactor: PresenterToInteractorCategoriesListProtocol?
    
    var router: PresenterToRouterCategoriesListProtocol?
    
    func fetchCategories() {
        
        self.interactor?.fetchCategoryList()
    }
    
    func displayShowsList(navigationController: UINavigationController) {
        
        let showListVC = ShowsListRouter.createModuleScreen()
        navigationController.pushViewController(showListVC, animated: true)
    }

}

extension CategoriesPresenter : InteractorToPresenterCategoriesListProtocol {
    
    func categoriesFetchedSuccess(categoriesArray: [String]) {
        
        self.view?.displayCategories(itemsArray: categoriesArray)
    }
    
    func categoriesFetchFailed() {
        
    }
    
    
    
}
