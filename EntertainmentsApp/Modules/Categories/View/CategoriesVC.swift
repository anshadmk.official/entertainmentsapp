//
//  CategoriesVC.swift
//  EntertainmentsApp
//
//  Created by Support on 02/02/20.
//  Copyright © 2020 Support. All rights reserved.
//

import UIKit

let kCategoriesVC = "CategoriesVC"

class CategoriesVC: UIViewController {

    var presenter:ViewToPresenterCategoriesListProtocol?
    
    @IBOutlet weak var categoriesTableView: UITableView!
    var categoryList = ["Movies", "Series"]
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        registerTableViewAssociates()
        self.presenter?.fetchCategories()

    }
    
    func registerTableViewAssociates() {
        
        self.categoriesTableView.dataSource = self
        self.categoriesTableView.delegate = self
        self.categoriesTableView.register(UINib(nibName: "CategoryTVC", bundle: nil), forCellReuseIdentifier: "Cell")
        
    }
}

extension CategoriesVC: PresenterToViewCategoriesListProtocol{
    
    func displayCategories(itemsArray: [String]) {
        
        categoryList = itemsArray
        categoriesTableView.reloadData()
    }
    
    
    func displayError() {
        
    }
}

extension CategoriesVC : UITableViewDataSource, UITableViewDelegate{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return categoryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.categoriesTableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CategoryTVC
        cell.categoryNameLabel.text = categoryList[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        (self.presenter ?? CategoriesPresenter()).displayShowsList(navigationController: self.navigationController!)
    }
    
}
