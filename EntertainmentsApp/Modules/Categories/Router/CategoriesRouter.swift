//
//  CategoriesRouter.swift
//  EntertainmentsApp
//
//  Created by Support on 02/02/20.
//  Copyright © 2020 Support. All rights reserved.
//

import UIKit

class CategoriesRouter: PresenterToRouterCategoriesListProtocol {
    
    
    static func createModuleScreen() -> CategoriesVC {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let view = storyboard.instantiateViewController(identifier: kCategoriesVC) as! CategoriesVC
        let interactor : PresenterToInteractorCategoriesListProtocol = CategoriesInteractor()
        let presenter : ViewToPresenterCategoriesListProtocol & InteractorToPresenterCategoriesListProtocol = CategoriesPresenter()
        let router : PresenterToRouterCategoriesListProtocol = CategoriesRouter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        return view
    }
    
    func pushToShowListScreen(navigationConroller: UINavigationController) {
        
    }
    

}
