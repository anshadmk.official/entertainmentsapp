//
//  CategoriesInteractor.swift
//  EntertainmentsApp
//
//  Created by Support on 02/02/20.
//  Copyright © 2020 Support. All rights reserved.
//

import UIKit

class CategoriesInteractor: PresenterToInteractorCategoriesListProtocol {

    weak var presenter: InteractorToPresenterCategoriesListProtocol?
    
    func fetchCategoryList() {
        
        let categoriesList = ["Movies", "Series"]
        self.presenter?.categoriesFetchedSuccess(categoriesArray: categoriesList)
    }
    

}
