//
//  MovieModel.swift
//  EntertainmentsApp
//
//  Created by Support on 02/02/20.
//  Copyright © 2020 Support. All rights reserved.
//

import UIKit


class Movies : NSObject, Codable {
    
    var results : [MovieModel]?
    var total_pages : Int?
    var page : Int?
    
    
    class func initWithData(data:Data) -> Movies?{
        
        let decoder = JSONDecoder()
        do {
            let object = try decoder.decode(Movies.self, from: data)
            return object
        } catch {
            print(error.localizedDescription)
        }
        
        return nil
    }
}



class MovieModel: NSObject, Codable {

    let popularity : Double?
    let vote_count : Int?
    let video : Bool?
    let poster_path : String?
    let id : Int?
    let adult : Bool?
    let backdrop_path : String?
    let original_language : String?
    let original_title : String?
    let title : String?
    let vote_average : Double?
    let overview : String?
    let release_date : String?
    
    
}
