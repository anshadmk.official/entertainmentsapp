//
//  SeriesModel.swift
//  EntertainmentsApp
//
//  Created by Support on 02/02/20.
//  Copyright © 2020 Support. All rights reserved.
//

import UIKit

class Serieses : NSObject, Codable {
    
    var results : [SeriesModel]?
    var total_pages : Int?
    var page : Int?
    
    
    class func initWithData(data:Data) -> Serieses?{
        
        let decoder = JSONDecoder()
        do {
            
            let object = try decoder.decode(Serieses.self, from: data)
            return object
        } catch {
            
            print(error.localizedDescription)
        }
        return nil
    }
}


class SeriesModel: NSObject, Codable {
    
    
    let popularity : Double?
    let vote_count : Int?
    let poster_path : String?
    let id : Int?
    let backdrop_path : String?
    let original_language : String?
    let vote_average : Double?
    let overview : String?
    let original_name : String?
    let name : String?
    let first_air_date : String?
    
}
