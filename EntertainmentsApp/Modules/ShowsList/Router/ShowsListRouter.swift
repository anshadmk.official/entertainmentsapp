//
//  CategoryItemsListRouter.swift
//  EntertainmentsApp
//
//  Created by Support on 01/02/20.
//  Copyright © 2020 Support. All rights reserved.
//

import UIKit

class ShowsListRouter: PresenterToRouterShowsListProtocol {
    
    func pushToShowDetailsScreen(navigationConroller: UINavigationController) {
        
    }
    

    
    static func createModuleScreen() -> ShowsListVC{
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let view = storyboard.instantiateViewController(identifier: kShowsListVCIdentifier) as! ShowsListVC
        let interactor : PresenterToInteractorShowsListProtocol = ShowsListInteractor()
        let presenter : ViewToPresenterShowsListProtocol & InteractorToPresenterShowsListProtocol = ShowsListPresenter()
        let router : PresenterToRouterShowsListProtocol = ShowsListRouter()
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        
        return view
    }
}
