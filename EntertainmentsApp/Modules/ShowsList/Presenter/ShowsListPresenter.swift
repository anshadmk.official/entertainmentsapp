//
//  CategoryItemsListPresenter.swift
//  EntertainmentsApp
//
//  Created by Support on 01/02/20.
//  Copyright © 2020 Support. All rights reserved.
//

import UIKit

enum CategoryName{
    
    case movie
    case tvSeries
    
}

enum CategoryGrouping{
    
    case upcomping
    case topRated
    case popular
    
}

class ShowsListPresenter : ViewToPresenterShowsListProtocol {
    
    var view: PresenterToViewShowsListProtocol?
    
    var interactor: PresenterToInteractorShowsListProtocol?
    
    var router: PresenterToRouterShowsListProtocol?
    
    func fetchDataItemsForCategory(category: CategoryName) {
        
        interactor?.fetchItemsFor(category: category, categoryGrouping: .popular)
    }
    
    func showDetailsViewController(navigationController: UINavigationController) {
        
    }
    
}

extension ShowsListPresenter : InteractorToPresenterShowsListProtocol {
    
    func showsFetchedSuccess(showsArray: Array<Any>, category: CategoryName) {
        
        self.view?.displayShows(itemsArray: showsArray)
    }
    
    func showsFetchFailed() {
        
    }
    
    
    
}
