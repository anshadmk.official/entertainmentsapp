//
//  ShowsListProtocols.swift
//  EntertainmentsApp
//
//  Created by Support on 02/02/20.
//  Copyright © 2020 Support. All rights reserved.
//

import UIKit

protocol ViewToPresenterShowsListProtocol: class{
    
    
    var view: PresenterToViewShowsListProtocol? {get set}
    var interactor: PresenterToInteractorShowsListProtocol? {get set}
    var router: PresenterToRouterShowsListProtocol? {get set}
    
    func fetchDataItemsForCategory(category:CategoryName)
    func showDetailsViewController(navigationController:UINavigationController)

}

protocol PresenterToViewShowsListProtocol: class{
    func displayShows(itemsArray:[Any])
    func showError()
}

protocol PresenterToRouterShowsListProtocol: class {
    static func createModuleScreen() -> ShowsListVC
    func pushToShowDetailsScreen(navigationConroller:UINavigationController)
}

protocol PresenterToInteractorShowsListProtocol: class {
    var presenter:InteractorToPresenterShowsListProtocol? {get set}
    func fetchItemsFor(category:CategoryName, categoryGrouping:CategoryGrouping)
}

protocol InteractorToPresenterShowsListProtocol: class {
    func showsFetchedSuccess(showsArray:Array<Any>, category:CategoryName)
    func showsFetchFailed()
}

