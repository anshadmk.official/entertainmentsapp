//
//  CategoriesItemsListInteractor.swift
//  EntertainmentsApp
//
//  Created by Support on 01/02/20.
//  Copyright © 2020 Support. All rights reserved.
//

import UIKit

class ShowsListInteractor: PresenterToInteractorShowsListProtocol {
    
    var presenter: InteractorToPresenterShowsListProtocol?
    let moviesListWebService = MoviesListWebServices()
    
    func fetchItemsFor(category: CategoryName, categoryGrouping : CategoryGrouping) {
        
        if category == .movie {
            
            switch categoryGrouping {
            case .popular :
                self.moviesListWebService.getPopularMovies(pageNumber: 1) { (items:[MovieModel]) in
                    
                    self.presenter?.showsFetchedSuccess(showsArray: items, category: category)
                }
            default:
                print("Invalid")
            }
        }
    }
}
