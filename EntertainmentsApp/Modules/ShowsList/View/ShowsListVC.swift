//
//  ShowsListVC.swift
//  EntertainmentsApp
//
//  Created by Support on 01/02/20.
//  Copyright © 2020 Support. All rights reserved.
//

import UIKit

let kShowsListVCIdentifier = "ShowsListVCIdentifier"



class ShowsListVC: UIViewController {
    
    @IBOutlet weak var categoryShowsListTableView: UITableView!
    var presenter : ViewToPresenterShowsListProtocol?
    var showsListArray = [Any]()
    var showsType : CategoryName?
    @IBOutlet weak var subCategoriesCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        showsType = .movie
        configureSegmentedControl()
        registerTableViewAssociates()
        
        presenter?.fetchDataItemsForCategory(category: .movie)
    
        
    }
    
    func registerTableViewAssociates(){
        
        categoryShowsListTableView.dataSource  = self
        categoryShowsListTableView.delegate = self
        categoryShowsListTableView.register(UINib(nibName: "ShowItemTVC", bundle: nil), forCellReuseIdentifier: "Cell")
    }
    
    func configureSegmentedControl() {
        
        var scrollViewWidth:Double = 0.0
        let items  = ["Top Rated", "Popular", "Upcoming","Latest"]
        for (index, element) in items.enumerated() {

            segmentedControl.setWidth(120, forSegmentAt: index)
            scrollViewWidth = scrollViewWidth + 120.0
        }
        self.scrollView.contentSize.width = CGFloat(scrollViewWidth)
    }
}



extension ShowsListVC : PresenterToViewShowsListProtocol {
    
    func displayShows(itemsArray: [Any]) {
        
        DispatchQueue.main.async {
            
            self.showsListArray = itemsArray
            self.categoryShowsListTableView.reloadData()
        }
        
    }
    
    func showError() {
        
        
    }
}

extension ShowsListVC : UITableViewDataSource, UITableViewDelegate{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return showsListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.categoryShowsListTableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ShowItemTVC
        
        let showItem = self.showsListArray[indexPath.row]

        if showsType == CategoryName.movie {

            let movieItem = showItem as! MovieModel
            cell.showNameLabel.text = movieItem.original_title

        }else if showsType == CategoryName.tvSeries {

            let seriesItem = showItem as! SeriesModel
            cell.showNameLabel.text = seriesItem.original_name
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 100
    }
    
}
