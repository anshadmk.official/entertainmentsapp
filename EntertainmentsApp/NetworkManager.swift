//
//  NetworkManager.swift
//  EntertainmentsApp
//
//  Created by Support on 02/02/20.
//  Copyright © 2020 Support. All rights reserved.
//

import UIKit

class NetworkManager: NSObject {
    
    static let shared = NetworkManager()
    
    
    private override init() {
        super.init()
    }
    
    func getRequest(url:String, completionHandeler:@escaping (_ response: Any? ,_ error: String?)-> Void) {
          
    
        var request = URLRequest(url: URL(string: url)!)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.timeoutInterval = TimeInterval(exactly: 30)!
      
      
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil);
        let task = session.dataTask(with: request) {
          (data, response, error) in
          
            completionHandeler(data, error?.localizedDescription)
      }
      task.resume()
  }


}
